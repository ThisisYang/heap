package heap

// Heap represents a heap structure
type Heap struct {
	data  []int
	count int
}

// New init a new heap
func New() *Heap {
	return &Heap{
		data: make([]int, 0),
	}
}

// Len return number of elements in heap
func (h *Heap) Len() int {
	return h.count
}

// Insert insert an int into heap
func (h *Heap) Insert(value int) {
	h.data = append(h.data, value)

	// idx is the index of inserted element in arr
	idx := h.count
	h.count++
	h.data = up(h.data, idx)

}

// Pop pops an element from heap
func (h *Heap) Pop() int {
	if h.count == 0 {
		return -1
	}
	// result is the first element of heap and will be popped
	result := h.data[0]

	h.data[0], h.data[h.count-1] = h.data[h.count-1], h.data[0]

	// cut the slice to exclude that element will be popped
	h.data = down(h.data[:h.count-1], 0)

	h.count--

	return result
}

func up(s []int, index int) []int {
	pIdx := parentIdx(index)
	if s[index] <= s[pIdx] {
		return s
	}
	s[index], s[pIdx] = s[pIdx], s[index]
	return up(s, pIdx)
}

func down(s []int, index int) []int {
	// case this is the leaf, dont swap
	if index >= len(s)/2 {
		return s
	}

	current := s[index]
	childs := childIdx(index)
	// if s[index] < s[child], get largest child, swap with parent
	var lchild, rchild, swapIdx int
	if childs[0] < len(s) {
		lchild = s[childs[0]]
	}
	if childs[1] < len(s) {
		rchild = s[childs[1]]
	}
	if lchild < current && rchild < current {
		return s
	}
	if lchild > rchild {
		swapIdx = childs[0]
	} else {
		swapIdx = childs[1]
	}

	// now swap between index and swapIdx
	s[index], s[swapIdx] = s[swapIdx], s[index]

	// recusrively call this method till either reach the leaf or larger than both children
	return down(s, swapIdx)

}

func parentIdx(index int) int {
	return (index - 1) / 2
}

func childIdx(index int) []int {
	return []int{index*2 + 1, index*2 + 2}
}

package stack

import (
	"errors"
	"sync"
)

var EmptyError = errors.New("Empty Stack")

type Stack struct {
	l       sync.Mutex // to make it thread safe, use a lock
	data    []interface{}
	current int
}

func New() *Stack {
	return &Stack{
		data: make([]interface{}, 10),
	}
}

func (s *Stack) IsEmpty() bool {
	s.l.Lock()
	defer s.l.Unlock()
	return len(s.data) == 0
}

// Push append the elements to the end
func (s *Stack) Push(e interface{}) {
	s.l.Lock()
	defer s.l.Unlock()

	s.data = append(s.data, e)
}

// Pop pops the last element
func (s *Stack) Pop() (interface{}, error) {
	s.l.Lock()
	defer s.l.Unlock()

	if len(s.data) == 0 {
		return nil, EmptyError
	}
	e := s.data[s.current]
	s.data = s.data[:len(s.data)-1]
	return e, nil
}

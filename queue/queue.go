package queue

import (
	"errors"
)

var ErrEmpty = errors.New("Empty Queue")

type Queue struct {
	data []interface{}
}

func newQueue() *Queue {
	return &Queue{
		data: make([]interface{}, 0),
	}
}

func (q *Queue) Len() int {
	return len(q.data)
}

func (q *Queue) IsEmpty() bool {
	return q.Len() == 0
}

func (q *Queue) Push(e interface{}) {
	q.data = append(q.data, e)
}

// Pop pops the element from head
func (q *Queue) Pop() (interface{}, error) {
	if q.Len() < 0 {
		return nil, ErrEmpty
	}
	e := q.data[0]
	q.data = q.data[1:]
	return e, nil
}

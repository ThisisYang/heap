package queue

import "errors"

var ErrFull = errors.New("Queue is full")

// CQueue is circular queue implemented using slice
// CQueue has fixed length.
type CQueue struct {
	data      []interface{}
	capacity  int
	writeHead int
	readHead  int
}

// NewCQueue initiate a new CQueue
// the actual underlying slice size = capacity+1
// cause we need 1 slice to check for queue full error
// since this is circular, we will rotate the data
// without extra slot, when writeHead == readHead, we can not diff if queue is full or empty
// with extra slot, if writeHead==readHead, it is empty
// when full, it is (writeHead+1)%capacity == readHead  (the capacity is requested capacity+1)
func NewCQueue(capacity int) *CQueue {
	return &CQueue{
		data:     make([]interface{}, capacity+1, capacity+1),
		capacity: capacity + 1,
	}
}
func (q *CQueue) Capacity() int {
	return q.capacity - 1
}

// Enqueue add an item into queue. return error if queue if full
func (q *CQueue) Enqueue(d interface{}) error {
	if (q.writeHead+1)%q.capacity == q.readHead {
		return ErrFull
	}
	q.data[q.writeHead] = d
	q.writeHead++
	return nil
}

func (q *CQueue) Dequeue() (interface{}, error) {
	if q.readHead == q.writeHead {
		return nil, ErrEmpty
	}
	item := q.data[q.readHead]
	q.readHead++
	return item, nil
}

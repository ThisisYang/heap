package mergesort

// MergeSort sorts the given slice
func MergeSort(s []int) {
	merge(s, 0, len(s)-1)
}

func merge(s []int, start, end int) {

	if start < end {
		mid := (start + end) / 2
		merge(s, start, mid)
		merge(s, mid+1, end)
		sort(s, start, end)
	}
}

// sort sorts the s[start, end+1]
// also, we are expecting that
func sort(s []int, start, end int) {

	// left and right should be sorted already
	if start >= end {
		return
	}
	mid := (end + start) / 2
	leftIdx, rightIdx := start, mid+1

	tmp := make([]int, 0)
	for leftIdx <= mid && rightIdx <= end {
		if s[leftIdx] < s[rightIdx] {
			tmp = append(tmp, s[leftIdx])
			leftIdx++
		} else {
			tmp = append(tmp, s[rightIdx])
			rightIdx++
		}
	}

	if leftIdx <= mid {
		tmp = append(tmp, s[leftIdx:mid+1]...)
	}

	if rightIdx <= end {
		tmp = append(tmp, s[rightIdx:end+1]...)
	}

	for _, v := range tmp {
		s[start] = v
		start++
	}
}

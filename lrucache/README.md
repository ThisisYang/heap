# LRU Cache

[LRU cache](https://leetcode.com/problems/lru-cache/description/) evicts least recent data if cache is full.

## Data Structure

In order to achieve `O(1)` lookup/insert/delete operations, use a `map` and a `doubly linked list (DLL)`.

`map` is used to store the cached data in key and a pointer to that node in doubly linked list.

`DLL` is used to track the recent usage. The most recent accessed value is `head.next` the least accessed `tail.pre`.

```golang
// suppose we are storeing the string in the cache
// Node represets the struct of each element in DLL
type node struct{
    key   string
    value string
    pre   *node
    next  *node
}

type dll struct{
    head *node
    tail *node
}

type LRU struct{
    data    map[string]*Node
    tracker *dll
}
```

## Implementation

when calling `LRU.Get(key)`, check if `LRU.data[key]` exists in `map`. If not, return `""`.
If exists, say `target := LRU.data[key]`, we need to first disconnect `target` node from `DLL`, reconnect `target.pre -> target.next`. Than go to the `dll.head`, reconnect `head -> target -> head.next` (since this is DLL, we need to work on `pre` pointer as well)

when calling `LRU.Put(key, value)`, first check if cache is full. If cache is not full, insert to `head.next` and add to `map`. If cache is full, first remove the first node from last, delete the removed element from `map`. Than, insert the new `*node` to `head.next` and insert it into `map`.

## Complexity

1. `Get`

For `Get` request, we first check the `map`, this is O(1). Than if neccessary, we maniputate the `DLL`. Both delete and insert in `DLL` is O(1). So the `Get` is O(1).

2. `Put`:

For `Put` request, we first insert into `DLL`, than remove least used from `DLL`. Both operation are related to either `head.next` or `tail.pre`. So it in O(1) for `DLL` manipulation. Than we add new node to `map` and remove old `node` if neccessary which are both O(1) operations. So the `Put` is O(1) as well.

package main

import (
	"fmt"
	"gitlab.com/practice/heap"
	"gitlab.com/practice/mergesort"
	"gitlab.com/practice/stack"
)

func main() {
	testMergeSort()
}

func testMergeSort() {
	s := []int{1, 2, 1, 3, 4}
	mergesort.MergeSort(s)
	fmt.Println(s)
}
func testStack() {
	s := stack.New()
	s.Push(1)
	s.Push(2)
	s.Pop()
	fmt.Println(s)
}
func testHeap() {
	h := heap.New()
	h.Insert(3)
	h.Insert(1)
	h.Insert(2)
	h.Insert(6)
	h.Insert(4)
	h.Insert(9)
	h.Insert(9)
	fmt.Println(h.Pop()) // expect 9
	h.Insert(5)
	h.Insert(7)
	fmt.Println(h.Pop()) // expect 9
	fmt.Println(h.Pop()) // expect 7
	fmt.Println(h.Len()) // expect 6
}

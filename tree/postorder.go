package tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func postorderTraversal(root *TreeNode) []int {
	// return recursive(root)
	return postStack(root)
}

// recursively traverse
func recursive(root *TreeNode) []int {
	if root == nil {
		return nil
	}
	result := make([]int, 0)
	if root.Left != nil {
		result = append(result, recursive(root.Left)...)
	}

	if root.Right != nil {
		result = append(result, recursive(root.Right)...)
	}

	result = append(result, root.Val)

	return result

}

// post order without recursion
// run a modified pre-order
// (visit right first than left, instead of visit right than left)
// after get modified pre-order result, reverse the values
func postStack(root *TreeNode) []int {
	reversed := preStack(root)
	result := make([]int, 0)
	for i := len(reversed) - 1; i >= 0; i-- {
		result = append(result, reversed[i])
	}
	return result
}

func preStack(root *TreeNode) []int {
	result := make([]int, 0)
	stack := make([]*TreeNode, 0)
	for root != nil || len(stack) > 0 {
		if root != nil {
			result = append(result, root.Val)
			stack = append(stack, root)
			root = root.Right
		} else {
			root, stack = stack[len(stack)-1], stack[:len(stack)-1]
			root = root.Left
		}
	}
	return result
}

package tree

// traverse the tree in pre order fasion.
// root, left, right

func preorderTraversal(root *TreeNode) []int {
	return preorderRecur(root)
}

// recursively traverse
func preorderRecur(root *TreeNode) []int {
	result := make([]int, 0)
	if root == nil {
		return result
	}
	result = append(result, root.Val)
	result = append(result, preorderTraversal(root.Left)...)
	result = append(result, preorderTraversal(root.Right)...)
	return result
}

// use stack.
// visit node first, than push to stack and node = node.Left
// if node == nil, node = stack[len(stack)-1].Right (Right of stack pop)
// exist when both root and stack is empty/nil
func preorderStack(root *TreeNode) []int {
	result := make([]int, 0)
	stack := make([]*TreeNode, 0)
	for root != nil || len(stack) > 0 {
		if root != nil {
			result = append(result, root.Val)
			stack = append(stack, root)
			root = root.Left
		} else {
			root, stack = stack[len(stack)-1].Right, stack[:len(stack)-1]
		}
	}

	return result
}

package tree

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

// in order traverse. virist left child than parent than right child
func inorderTraversal(root *TreeNode) []int {

	return traverseStack(root)
}

// traverse the tree without recursive
// use stack to track root.
// first keep visit node.Left, push it to stack
// if node is nil, pop the last from stack and add its value to result
// then set the root to node.Right which visit the right subtree
// if both stack and root are empty/nil, it means we
func traverseStack(root *TreeNode) []int {
	result := make([]int, 0)
	stack := make([]*TreeNode, 0)
	for root != nil || len(stack) > 0 {
		if root != nil {
			stack = append(stack, root)
			root = root.Left
		} else {
			root, stack = stack[len(stack)-1], stack[:len(stack)-1]
			result = append(result, root.Val)
			root = root.Right
		}
	}
	return result
}
func traverseRecur(root *TreeNode) []int {
	if root == nil {
		return nil
	}
	r := make([]int, 0)
	if root.Left != nil {
		sub := traverseRecur(root.Left)
		r = append(r, sub...)
	}
	r = append(r, root.Val)

	if root.Right != nil {
		rightsub := traverseRecur(root.Right)
		r = append(r, rightsub...)
	}
	return r
}
